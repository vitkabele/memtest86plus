/**
 * @brief Support for rendering characters on graphical mode displays
 *  (useful for EFI enabled machines without text-mode displays)
 *
 * @file display.c
 * @author Vit Kabele <vit@kabele.me>
 * @date 30.12.2021
 */


#ifndef MEMTEST86_5_01_DISPLAY_H
#define MEMTEST86_5_01_DISPLAY_H

#include <stdint.h>

void
init_display(uint64_t addr, uint32_t bpp, uint32_t width, uint32_t height);

void
display_print(uint32_t y, uint32_t x, const char *text);

void
display_clear_screen(void);

void
set_indexed_colours(uint8_t red_value, uint8_t green_value, uint8_t blue_value);

void
set_rgb_colours(uint8_t red_field, uint8_t red_mask_size,
                uint8_t green_field, uint8_t green_mask_size,
                uint8_t blue_field, uint8_t blue_mask_size);

#endif //MEMTEST86_5_01_DISPLAY_H
