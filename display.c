/**
 * @brief Support for rendering characters on graphical mode displays
 *  (useful for EFI enabled machines without text-mode displays)
 *
 * @file display.c
 * @author Vit Kabele <vit@kabele.me>
 * @date 30.12.2021
 */

/* ------------------------- FILE INCLUSION -------------------------------- */

#include "display.h"
#include "screen_buffer.h"

/* ------------------- CONSTANT / MACRO DEFINITIONS ------------------------ */

#define FONT_WIDTH 8
#define FONT_HEIGHT 16

/**
 * Minimum required dimensions in pixels of the graphical console
 */
#define DISPLAY_MIN_HEIGHT (SCREEN_Y * FONT_HEIGHT)
#define DISPLAY_MIN_WIDTH (SCREEN_X * FONT_WIDTH)

/**
 * Create a mask with n bits set to one
 */
#define VALUE_MASK(n) ((1ull << ((n) + 1)) - 1)

#define MAX(a, b) ((a) > (b)) ? (a) : (b)

#define COLOR_BG 0
#define COLOR_FG 1

/* Two basic colours. Independently on colour encoding used
 * by the framebuffer, the black is always zero and white
 * is always all zeroes
 */
#define COLOR_WHITE UINT64_MAX
#define COLOR_BLACK 0x0

/* ------------------------ TYPE DECLARATIONS ------------------------------ */

typedef struct framebuffer_info_str {

    /** Screen dimensions and bits per pixel */
    uint32_t width, height, bpp, address;

    /** Bit mask must be bigger than highest possible value of bpp */
    uint64_t bit_mask;

    /**
     * Horizontal and vertical scale
     *
     * Based on the width and height of the screen, the font might be scaled
     * up to better use the screen space.
     */
    uint32_t h_scale, v_scale;

    /**
     * In case that the scaled content is smaller than screen, we want to move
     * the content to the screen centre.
     */
    uint32_t h_offset, v_offset;

    uint8_t initialized;
} framebuffer_info_t;

/* ----------------------- OBJECT DECLARATIONS ----------------------------- */

framebuffer_info_t framebuffer_info = {.initialized = 0};
extern const char* get_font(void);

/**
 * Provide color descriptors for foreground and background
 */
uint64_t color_theme[] = {
        [COLOR_BG] = COLOR_BLACK,
        [COLOR_FG] = COLOR_WHITE
};

/* ----------------------- LOCAL FUNCTIONS ---------------------------------- */

/**
 * @brief Render the pixel value on the specific coordinates
 *
 * @param this Pointer to the framebuffer structure
 * @param x Horizontal coordinate
 * @param y Vertical coordinate
 * @param value Pixel value to render
 *
 * @retval -1 Attempt to display pixel out of display bounds
 * @retval 0 Success
 */
static int
draw_pixel(framebuffer_info_t *const this, uint32_t x, uint32_t y, uint64_t value) {
    if (x > this->width || y > this->height) {
        /* Out of bounds */
        return -1;
    }
    const uint32_t pixel_offset = (y * this->width) + x;
    const uint32_t bit_offset = pixel_offset * this->bpp;
    const uint32_t byte_offset = bit_offset / 8;
    const uint32_t bits_modulo = bit_offset % 8;
    const uint64_t masked_value = value & this->bit_mask;
    /* Mask for clearing out the pixel bits */
    const uint64_t shifted_mask = (this->bit_mask << bits_modulo);
    volatile uint64_t *pixel_location = (void*) (framebuffer_info.address + byte_offset);
    /* Read current pixel values from framebuffer and clean pixels for the current pixel */
    const uint64_t old_data = *pixel_location & ~shifted_mask;
    *pixel_location = (masked_value << bits_modulo) | old_data;
    return 0;
}

/* ------------------ GLOBAL FUNCTION DEFINITIONS -------------------------- */

/**
 * Set the whole screen to black color.
 *
 * It is not called clear screen, because there already is function with this name
 * for EGA mode displays.
 */
void
display_clear_screen(void) {
    for (uint32_t x = 0; x < framebuffer_info.width; ++x) {
        for (uint32_t y = 0; y < framebuffer_info.height; ++y) {
            draw_pixel(&framebuffer_info, x, y, color_theme[COLOR_BG]);
        }
    }
}

/**
 * @brief Initialize the graphical mode display
 *
 * @note Despite its name, this function does not initialize anything. It expects that
 * the framebuffer was already initialized (i.e. by multiboot loader) and just saves all
 * the information for later use.
 *
 * @param addr Framebuffer address
 * @param bpp Bits per pixel
 * @param width Width of the screen in pixels
 * @param height Height of the screen in pixels
 */
void
init_display(uint64_t addr, uint32_t bpp, uint32_t width, uint32_t height)
{
    framebuffer_info.address = addr;
    framebuffer_info.width = width;
    framebuffer_info.height = height;
    framebuffer_info.bpp = bpp;
    framebuffer_info.bit_mask = VALUE_MASK(bpp);

    /* If a dimension of the screen is integral multiple of the minimum required
     * screen width, we scale the content with the appropriate ratio to take advance
     * of the bigger screen. We must take maximum from 1 and the calculated value for
     * the cases where screen is smaller than required minimum to prevent scaling factor
     * of zero. In that case we bail out to one, even if the content is not going to fit
     * the screen. */
    framebuffer_info.h_scale = MAX(1, width / DISPLAY_MIN_WIDTH);
    framebuffer_info.v_scale = MAX(1, height / DISPLAY_MIN_HEIGHT);

    /* Calculate how many pixels remains unused with actual scaling factor and divide it
     * with two. This will create even paddings between all screen edges and content */
    framebuffer_info.h_offset = (width - (framebuffer_info.h_scale * DISPLAY_MIN_WIDTH)) / 2;
    framebuffer_info.v_offset = (height - (framebuffer_info.v_scale * DISPLAY_MIN_HEIGHT)) / 2;

    color_theme[COLOR_BG] = 0x0;
    color_theme[COLOR_FG] = UINT64_MAX;

    framebuffer_info.initialized = 1;
}

void
set_rgb_colours(uint8_t red_field, uint8_t red_mask_size,
                uint8_t green_field, uint8_t green_mask_size,
                uint8_t blue_field, uint8_t blue_mask_size)
{
    color_theme[COLOR_FG] = UINT64_MAX;
    color_theme[COLOR_BG] = VALUE_MASK(blue_mask_size) << blue_field;
}

/**
 * Render a character on screen
 *
 * It renders a character on
 *
 * @note This function depends on compiled-in font
 *
 * @param x Column of characters on the screen
 * @param y Row of characters on the screen
 * @param c Character to print
 */
void
render_char(uint32_t x, uint32_t y, unsigned char c) {
    if (!framebuffer_info.initialized) return;
    /* Calculate the coordinate of upper left pixel w.r.t. font dimensions */
    const uint32_t x_pix = x * FONT_WIDTH * framebuffer_info.h_scale + framebuffer_info.h_offset;
    const uint32_t y_pix = y * FONT_HEIGHT * framebuffer_info.v_scale + framebuffer_info.v_offset;
    const uint32_t font_offset = c * FONT_HEIGHT;
    const char *font = get_font();

    for (int i = 0; i < FONT_HEIGHT * framebuffer_info.v_scale; ++i) {
        const uint8_t line = font[font_offset + i/framebuffer_info.v_scale];
        for (int j = 0; j < FONT_WIDTH * framebuffer_info.h_scale; ++j) {
            /* The offset to the mask of active bits. Redundant in case of scaling
             * factor one, but required for scaling > 1 */
            const uint8_t mask_offset = j/framebuffer_info.h_scale;
            const uint8_t mask = (1u << (FONT_WIDTH - mask_offset - 1));
            /* Use white for active bits and black out the inactive bits */
            draw_pixel(&framebuffer_info, x_pix + j, y_pix + i, (line & mask) ? color_theme[COLOR_FG] : color_theme[COLOR_BG]);
        }
    }
}

/**
 * Print a string on display
 *
 * @param x Starting character column on the screen
 * @param y Starting character line on the screen
 * @param text Zero terminated string to print
 */
void
display_print(uint32_t y, uint32_t x, const char *text) {
    for (uint32_t i = 0; text[i]; ++i) {
        render_char(x + i, y, text[i]);
    }
}